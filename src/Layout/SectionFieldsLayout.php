<?php

declare(strict_types = 1);

namespace Drupal\layout_section_fields\Layout;

use Drupal\Core\Layout\LayoutDefault;
use Drupal\Core\Plugin\PluginFormInterface;

/**
 * Override the default layout to inject any custom fields.
 */
class SectionFieldsLayout extends LayoutDefault implements PluginFormInterface {

  /**
   * {@inheritdoc}
   */
  public function build(array $regions) {
    $build = parent::build($regions);

    return $build;
  }

}
