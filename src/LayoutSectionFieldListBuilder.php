<?php

namespace Drupal\layout_section_fields;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;

/**
 * Provides a listing of Layout section field entities.
 */
class LayoutSectionFieldListBuilder extends ConfigEntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['label'] = $this->t('Layout section field');
    $header['id'] = $this->t('Machine name');
    $header['layouts'] = $this->t('Layouts');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    $layouts = $entity->getLayouts();
    $row['label'] = $entity->label();
    $row['id'] = $entity->id();
    $row['layouts'] = (!empty($layouts) ? trim(implode(",", $layouts)) : $this->t('None selected, all layouts'));

    return $row + parent::buildRow($entity);
  }

}
