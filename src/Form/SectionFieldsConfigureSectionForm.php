<?php

declare(strict_types = 1);

namespace Drupal\layout_section_fields\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\layout_builder\Form\ConfigureSectionForm;
use Drupal\layout_builder\SectionStorageInterface;
use Drupal\layout_section_fields\LayoutSectionFieldLimits;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Override the default ConfigureSectionForm to inject fields into the form.
 */
class SectionFieldsConfigureSectionForm extends ConfigureSectionForm {

  /**
   * Entity storage for the layout section fields.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $layoutSectionStorage;

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->layoutSectionStorage = $container->get('entity_type.manager')->getStorage('layout_section_field');
    return $instance;
  }

  /**
   * A constant provider name for third party settings.
   */
  const PROVIDER = 'layout_section_fields';

  /**
   * {@inheritDoc}
   */
  public function buildForm(
    array $form,
    FormStateInterface $form_state,
    SectionStorageInterface $section_storage = NULL,
    $delta = NULL,
    $plugin_id = NULL
  ): array {
    $plugin = $plugin_id;
    // Get the parent form.
    $form = parent::buildForm($form, $form_state, $section_storage, $delta, $plugin_id);

    $settings = [];

    if ($this->isUpdate) {
      $section = $this->sectionStorage
        ->getSection($this->delta);

      $plugin = $section->getLayoutId();
      $settings = $section->getThirdPartySettings(self::PROVIDER);
    }

    $sectionFields = $this->getLayoutSectionFields($plugin);

    if (!empty($sectionFields)) {
      $form[self::PROVIDER] = [
        '#type' => 'container',
      ];

      foreach ($sectionFields as $key => $entity) {
        $fieldType = $entity->getFieldType();

        $formType = LayoutSectionFieldLimits::ALLOWED_FIELD_TYPE_MAPPING[$fieldType];

        $form[self::PROVIDER][$key] = [
          '#type' => $formType,
          '#title' => $entity->label(),
          '#default_value' => $settings[$key]['value'] ?? '',
          '#format' => $settings[$key]['format'] ?? 'basic_html',
        ];
      }
    }

    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    // Call the parent last so the settings get saved.
    parent::submitForm($form, $form_state);
    // Save the extra fields.
    $values = $form_state->getValue(self::PROVIDER);

    foreach ($values as $key => $value) {
      // These are plain text fields, convert them to match a formatted text.
      $storageValue = $value;
      if (!is_array($value)) {
        $storageValue = [
          'value' => $value,
          'format' => 'plain_text',
        ];
      }

      // @todo The storage will need to change in V2 whenever we implement
      // field plugins. This works for text fields for now.
      $this->sectionStorage
        ->getSection($this->delta)
        ->setThirdPartySetting(self::PROVIDER, $key, $storageValue);
    }

    $config = $this->layout->getConfiguration();
    $settings = $this->sectionStorage
      ->getSection($this->delta)
      ->getThirdPartySettings(self::PROVIDER);

    $config[self::PROVIDER] = $settings;

    $this->sectionStorage
      ->getSection($this->delta)
      ->setLayoutSettings($config);

    // Save the new section settings.
    $this->layoutTempstoreRepository->set($this->sectionStorage);
  }

  /**
   * Load the layout fields.
   *
   * @param string $pluginId
   *   The layout plugin id to filter fields.
   *
   * @return array
   *   Array of entities.
   */
  public function getLayoutSectionFields(string $pluginId): array {
    $entities = $this->layoutSectionStorage->loadMultiple();

    foreach ($entities as $id => $entity) {
      $layouts = $entity->getLayouts();
      if (!is_null($layouts)) {
        if (!array_key_exists($pluginId, $layouts)) {
          $entities[$id] = NULL;
        }
      }
    }

    return array_filter($entities);
  }

}
