<?php

namespace Drupal\layout_section_fields\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Field\FieldTypePluginManagerInterface;
use Drupal\Core\Field\WidgetPluginManager;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Layout\LayoutPluginManagerInterface;
use Drupal\layout_section_fields\LayoutSectionFieldLimits;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provide a form to allow a user to add new fields to sections.
 */
class LayoutSectionFieldForm extends EntityForm {

  /**
   * The plugin.manager.field.field_type service.
   *
   * @var \Drupal\Core\Field\FieldTypePluginManagerInterface
   */
  protected $fieldTypePluginManager;

  /**
   * The plugin.manager.field.widget service.
   *
   * @var \Drupal\Core\Field\WidgetPluginManager
   */
  protected $widgetPluginManager;

  /**
   * The plugin.manager.core.layout service.
   *
   * @var \Drupal\Core\Layout\LayoutPluginManagerInterface
   */
  protected $layoutDiscovery;

  /**
   * The allowed field types the user can choose.
   *
   * @var array
   */
  private $fieldTypeLimits;

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.field.field_type'),
      $container->get('plugin.manager.field.widget'),
      $container->get('plugin.manager.core.layout')
    );
  }

  /**
   * Constructor for the form class.
   *
   * @param \Drupal\Core\Field\FieldTypePluginManagerInterface $field_type_plugin_manager
   *   The plugin.manager.field.field_type service.
   * @param \Drupal\Core\Field\WidgetPluginManager $widget_plugin_manager
   *   The plugin.manager.field.widget service.
   * @param \Drupal\Core\Layout\LayoutPluginManagerInterface $layout_discovery
   *   The plugin.manager.core.layout service.
   */
  public function __construct(
    FieldTypePluginManagerInterface $field_type_plugin_manager,
    WidgetPluginManager $widget_plugin_manager,
    LayoutPluginManagerInterface $layout_discovery
  ) {
    $this->fieldTypePluginManager = $field_type_plugin_manager;
    $this->widgetPluginManager = $widget_plugin_manager;
    $this->layoutDiscovery = $layout_discovery;
    $this->fieldTypeLimits = array_keys(LayoutSectionFieldLimits::ALLOWED_FIELD_TYPE_MAPPING);
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state): array {
    $form = parent::form($form, $form_state);

    /** @var \Drupal\layout_section_fields\Entity\LayoutSectionFieldInterface $entity */
    $entity = $this->entity;
    $fieldType = $entity->getFieldType();

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Field name'),
      '#maxlength' => 255,
      '#default_value' => $entity->label(),
      '#description' => $this->t('Field name for the Layout section field.'),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $entity->id(),
      '#machine_name' => [
        'exists' => '\Drupal\layout_section_fields\Entity\LayoutSectionField::load',
      ],
      '#disabled' => !$entity->isNew(),
    ];

    // Get the available fields from the field plugin manager.
    // Gather valid field types.
    $field_type_options = [];
    foreach ($this->fieldTypePluginManager->getGroupedDefinitions($this->fieldTypePluginManager->getUiDefinitions()) as $category => $field_types) {
      foreach ($field_types as $name => $field_type) {
        if (in_array($name, $this->fieldTypeLimits)) {
          $field_type_options[$category][$name] = $field_type['label'];
        }
      }
    }

    // @todo Add plugin settings based on the field widget.
    $form['field_type'] = [
      '#title' => $this->t('Field type'),
      '#empty_option' => $this->t('- Select a field type -'),
      '#type' => 'select',
      '#options' => $field_type_options,
      '#required' => TRUE,
      '#default_value' => $fieldType,
    ];

    if (!empty($fieldType)) {
      // @todo Add option to load plugin widget settings.
    }

    $form['allowed_layouts'] = [
      '#type' => 'checkboxes',
      '#options' => $this->getLayoutOptions(),
      '#required' => FALSE,
      '#title' => $this->t('Allowed layouts'),
      '#default_value' => $entity->getLayouts() ?? [],
      '#description' => $this->t('Select the layouts to which this field should apply. Leave empty for all layouts.'),
    ];

    return $form;
  }

  /**
   * Get the available layouts in the site.
   *
   * @return array
   *   Array of options for use in a form element.
   */
  private function getLayoutOptions(): array {
    $options = [];
    $layouts = $this->layoutDiscovery->getLayoutOptions();
    foreach ($layouts as $group => $layout) {
      // The first layout returned has no group and is null,
      // let's filter it out.
      $layout = array_filter($layout);

      if (!empty($layout)) {
        foreach ($layout as $key => $label) {
          $options[$key] = $label;
        }
      }
    }

    return $options;
  }

  /**
   * {@inheritDoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    /** @var \Drupal\layout_section_fields\Entity\LayoutSectionFieldInterface $entity */
    $entity = $this->entity;
    $entity->setFieldType($form_state->getValue('field_type'));
    $entity->setLayouts(array_filter($form_state->getValue('allowed_layouts')));
    $status = $entity->save();

    switch ($status) {
      case SAVED_NEW:
        $this->messenger()->addMessage($this->t('Created the %label Layout section field.', [
          '%label' => $entity->label(),
        ]));
        break;

      default:
        $this->messenger()->addMessage($this->t('Saved the %label Layout section field.', [
          '%label' => $entity->label(),
        ]));
    }
    $form_state->setRedirectUrl($entity->toUrl('collection'));
  }

}
