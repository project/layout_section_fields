<?php

namespace Drupal\layout_section_fields\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Layout section field entities.
 */
interface LayoutSectionFieldInterface extends ConfigEntityInterface {

  /**
   * Get the field type that was set for the entity.
   *
   * @return string|null
   *   The type of field or null if no field is expected.
   */
  public function getFieldType(): ?string;

  /**
   * Set the field type variable.
   *
   * @param string $type
   *   The machine name of the field type.
   *
   * @return \Drupal\layout_section_fields\Entity\LayoutSectionFieldInterface
   *   Return the entity.
   */
  public function setFieldType(string $type): LayoutSectionFieldInterface;

  /**
   * Set the layouts that this field should be limited.
   *
   * @param array $layouts
   *   Array of layouts keyed by layout id.
   *   e.g. ['layout_onecol' => 'layout_onecol'].
   *
   * @return \Drupal\layout_section_fields\Entity\LayoutSectionFieldInterface
   *   Return the entity.
   */
  public function setLayouts(array $layouts): LayoutSectionFieldInterface;

  /**
   * Get the layouts that were set for the field.
   *
   * @return array|null
   *   Return the array of layouts that this field should be limited.
   */
  public function getLayouts(): ?array;

}
