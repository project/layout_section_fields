<?php

namespace Drupal\layout_section_fields\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;

/**
 * Defines the Layout section field entity.
 *
 * @ConfigEntityType(
 *   id = "layout_section_field",
 *   label = @Translation("Layout section field"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\layout_section_fields\LayoutSectionFieldListBuilder",
 *     "form" = {
 *       "add" = "Drupal\layout_section_fields\Form\LayoutSectionFieldForm",
 *       "edit" = "Drupal\layout_section_fields\Form\LayoutSectionFieldForm",
 *       "delete" = "Drupal\layout_section_fields\Form\LayoutSectionFieldDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\layout_section_fields\LayoutSectionFieldHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "layout_section_field",
 *   admin_permission = "administer site configuration",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "fieldType",
 *     "layouts"
 *   },
 *   links = {
 *     "canonical" = "/admin/config/content/layout_section_fields/field/{layout_section_field}",
 *     "add-form" = "/admin/config/content/layout_section_fields/field/add",
 *     "edit-form" = "/admin/config/content/layout_section_fields/field/{layout_section_field}/edit",
 *     "delete-form" = "/admin/config/content/layout_section_fields/field/{layout_section_field}/delete",
 *     "collection" = "/admin/config/content/layout_section_fields"
 *   }
 * )
 */
class LayoutSectionField extends ConfigEntityBase implements LayoutSectionFieldInterface {

  /**
   * The Layout section field ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Layout section field label.
   *
   * @var string
   */
  protected $label;

  /**
   * The machine name of the field type.
   *
   * @var string
   */
  protected $fieldType;

  /**
   * Array of layouts that this field should apply. Null if no limit.
   *
   * @var array|null
   */
  protected $layouts;

  /**
   * {@inheritDoc}
   */
  public function getFieldType(): ?string {
    return $this->fieldType;
  }

  /**
   * {@inheritDoc}
   */
  public function setFieldType(string $type): LayoutSectionFieldInterface {
    $this->fieldType = $type;
    return $this;
  }

  /**
   * {@inheritDoc}
   */
  public function getLayouts(): ?array {
    return $this->layouts ?: NULL;
  }

  /**
   * {@inheritDoc}
   */
  public function setLayouts(array $layouts): LayoutSectionFieldInterface {
    $this->layouts = [];
    if (!empty($layouts)) {
      $this->layouts = $layouts;
    }
    return $this;
  }

}
