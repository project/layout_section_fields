<?php

declare(strict_types = 1);

namespace Drupal\layout_section_fields;

/**
 * Shared class to provide constants across the project.
 */
final class LayoutSectionFieldLimits {

  // Define the allowed field types and their form render element.
  // This is temporary and will eventually be replaced
  // with field widget settings.
  const ALLOWED_FIELD_TYPE_MAPPING = [
    'text_long' => 'text_format',
    'string' => 'textfield',
    'string_long' => 'textarea',
  ];

}
