<?php

declare(strict_types = 1);

namespace Drupal\layout_section_fields;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * Alter routes for the layout_builder_section_fields module.
 */
class RouteSubscriber extends RouteSubscriberBase {

  const DEFAULT_FORM = '\Drupal\layout_section_fields\Form\SectionFieldsConfigureSectionForm';

  /**
   * {@inheritDoc}
   */
  protected function alterRoutes(RouteCollection $collection): void {
    // Change the form class for configuring sections.
    if ($route = $collection->get('layout_builder.configure_section')) {
      $route->setDefault('_form', self::DEFAULT_FORM);
    }
  }

}
