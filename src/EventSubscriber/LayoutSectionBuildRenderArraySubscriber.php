<?php

declare(strict_types = 1);

namespace Drupal\layout_section_fields\EventSubscriber;

use Drupal\layout_builder\Event\PrepareLayoutEvent;
use Drupal\layout_builder\LayoutBuilderEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Event subscriber for the buildSection layout builder event.
 */
class LayoutSectionBuildRenderArraySubscriber implements EventSubscriberInterface {

  /**
   * Add custom fields to the section render array.
   */
  public function buildSection(PrepareLayoutEvent $event): void {
    $storage = $event->getSectionStorage();
    $sections = $storage->getSections();

    foreach ($sections as $section) {
      $settings = $section->getThirdPartySettings('layout_section_fields');
      if (!empty($settings)) {
        $layoutSettings = $section->getLayoutSettings();
        // @todo Use appendComponent to push the text to a region?
        foreach ($settings as $key => $value) {
          $layoutSettings['layout_section_fields'][$key] = $value;
        }

        $section->setLayoutSettings($layoutSettings);
      }
    }
  }

  /**
   * {@inheritDoc}
   */
  public static function getSubscribedEvents(): array {
    return [
      LayoutBuilderEvents::PREPARE_LAYOUT => 'buildSection',
    ];
  }

}
