# Layout Section Fields

The layout section fields module provides some missing functionality from
Drupal Core's Layout Builder. This will allow a site builder to create new
fields through the UI, which will then be injected into the section configuration
form in Layout Builder.


## Configuration
1. Enable the module.
2. Browse to /admin/config/content/layout_section_fields
3. Add the fields you would like appended to your layout builder sections
4. Edit a layout and configure a new section
    a. You should see the fields you created in step 3.
5. Save the configuration and layout.

_note_ You will NOT see your fields immediately. You _must_ edit your twig
files for the region to be printed.

6. Edit all of your layout.html.twig files and print the new region like this:
```
{% if content.layout_section_fields %}
  <div {{ region_attributes.first.addClass('layout__region', 'layout__region--layout--section-fields') }}>
    {{ content.layout_section_fields }}
  </div>
{% endif %}
```

7. You can also target specific fields in twig with:
```
{{ content.layout_section_fields.{ENTITY_MACHINE_NAME} }}
```
